package issue1;

import java.util.Scanner;

public class Issue1Application {

	public static void main(String[] args) {
//		Scanner refScanner = new Scanner(System.in);
//		
//		System.out.println("Enter password: ");
//		char[] password = refScanner.next().toCharArray();
//		
//		char[] hidePassword = new char[password.length];
//		for (int i = 0; i < password.length; i++) {
//			hidePassword[i] = 'x';
//		}
//		
//		System.out.println(password);
//		System.out.println(hidePassword);
		
		
		try {
			System.out.println("Enter your name: ");
			String name = System.console().readLine();
			
			System.out.println("Enter Password: ");
			char[] password = System.console().readPassword();
			
			char[] hiddenPassword = new char[password.length];
			for (int i = 0; i < password.length; i++) {
				hiddenPassword[i] = 'x';
			}
			//String hidepass = String.valueOf(password);
			
			System.out.println("\n"+name);
			
			System.out.println(hiddenPassword);
			
		} catch (NullPointerException e) {
			System.out.println(e + " because you are using IDE." );
		}
	}

}
