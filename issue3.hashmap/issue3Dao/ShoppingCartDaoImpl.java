package issue3Dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import issue3Pojo.Product;

public class ShoppingCartDaoImpl implements ShoppingCartDaoInterface {
	List<Product> catalog = List.of(new Product(101, "Laptop", 1777.77), new Product(102, "Desktop", 1888.88));

	// Product = key, Integer = value (quantity; the count of key)
	Map<Product, Integer> productInfo = new HashMap<>();	

	public Product fetchProductById(int pid) {
		for (Product product : catalog) {
			if (product.getPid() == pid) {
				return product;
			} 
		}
		return null;
	}

	@Override
	public void insertProduct(int pid) {
		Product fetchProduct = fetchProductById(pid);

		if (productInfo.containsKey(fetchProduct)) {
			productInfo.put(fetchProduct, productInfo.get(fetchProduct) + 1);
		} else {
			productInfo.put(fetchProduct, 1);
		}

		//System.out.println(productInfo);
		System.out.println("Item added to cart!");
	}

	@Override
	public void deleteProduct(int pid) {
		Product fetchProduct = fetchProductById(pid);

//		for (Map.Entry<Product, Integer> refEntrySet : productInfo.entrySet()) {
//			// System.out.println("Key: " +refEntrySet.getKey());
//			// System.out.println("Value: " +refEntrySet.getValue());
//			if (refEntrySet.getValue() > 1) {
//				
//				productInfo.put(fetchProduct, productInfo.get(fetchProduct) - 1);
//				// System.out.println("if Value > 1: " + refEntrySet.getValue());
//			} else if (refEntrySet.getValue() == 1) {
//				// System.out.println("Before remove: " +refEntrySet.getValue());
//				productInfo.remove(fetchProduct);
//				// System.out.println("After remove: " +refEntrySet.getValue());
//			}
//		}
		//--
		//
		for (Map.Entry<Product, Integer> refEntrySet : productInfo.entrySet()) {
			if (refEntrySet.getValue() > 1) {
				productInfo.put(fetchProduct, productInfo.get(fetchProduct) - 1);
			} else { 
				productInfo.entrySet().removeIf(e -> e.getValue() == 1);
			}
		}
		
		//System.out.println(productInfo);
		System.out.println("Item removed from cart!");
	}

	@Override
	public void viewProduct() {
		for (Map.Entry<Product, Integer> refEntrySet : productInfo.entrySet()) {
			System.out.println(refEntrySet.getKey() + ", Quantity: " + refEntrySet.getValue());
		}
		System.out.println("\n");
	}

	@Override
	public void checkoutCart() {
		double totalPrice = 0;

		for (Map.Entry<Product, Integer> refEntrySet : productInfo.entrySet()) {
			totalPrice += refEntrySet.getKey().getProductPrice() * refEntrySet.getValue();

		}
		System.out.println("All items checked out, cart has been cleared.");
		System.out.printf("The total price is: %.2f", totalPrice);
		productInfo.clear();
	}

	@Override
	public void clearCart() {
		productInfo.clear();
		System.out.println("Cart has been cleared.");
	}
}
