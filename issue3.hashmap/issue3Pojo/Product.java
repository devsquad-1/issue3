package issue3Pojo;

public class Product {
	private int pid;
	private String productName;
	private double productPrice;

	//
	public Product() {

	}

	public Product(int pid, String productName, double productPrice) {
		this.pid = pid;
		this.productName = productName;
		this.productPrice = productPrice;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
//		return "Product ID: " + pid + " Product Name: " + productName + " Product Price: $" +productPrice
//				+ " Quantity: " + quantity;
		return "Product ID: " + pid + " Product Name: " + productName + " Product Price: $" + productPrice;
	}

}
