package issue3Service;

public interface ShoppingCartServiceInterface {
	void menu();
	void insertProduct();
	void deleteProduct();
	void viewProducts();
	void checkoutCart();
	void clearCart();
}
