package issue3Service;

import java.util.Scanner;

import issue3Dao.*;
import issue3Pojo.Product;

public class ShoppingCartServiceImpl implements ShoppingCartServiceInterface {

	ShoppingCartDaoInterface refShoppingCartDaoImpl = new ShoppingCartDaoImpl();
	Product refCartItem;
	Scanner refScanner = new Scanner(System.in);

	@Override
	public void menu() {

		boolean exit = false;
		do {
			System.out.println("1. Add item to cart");
			System.out.println("2. Remove an item from cart");
			System.out.println("3. View items in cart");
			System.out.println("4. Checkout");
			System.out.println("5. Empty cart");
			System.out.println("6. Exit program");

			System.out.println("\nEnter your choice: ");
			int choice = refScanner.nextInt();

			switch (choice) {
			case 1:
				insertProduct();
				break;

			case 2:
				deleteProduct();
				break;

			case 3:
				viewProducts();
				break;
			case 4:
				checkoutCart();
				exit = true;
				break;
			case 5:
				clearCart();
				break;
			case 6:
				exit = true;
				System.out.println("You have ended the program. Thank you!");
				break;
			default:
				break;
			}
		} while (!exit);

	}

	@Override
	public void insertProduct() {

		System.out.println("Enter product ID to add to cart: ");
		int pid = refScanner.nextInt();
		refShoppingCartDaoImpl.insertProduct(pid);

	}

	@Override
	public void deleteProduct() {
		System.out.println("Enter product ID to remove from cart: ");
		int pid = refScanner.nextInt();

		refShoppingCartDaoImpl.deleteProduct(pid);
	}

	@Override
	public void viewProducts() {
		System.out.println("Items in cart: ");
		refShoppingCartDaoImpl.viewProduct();
	}

	@Override
	public void checkoutCart() {
		refShoppingCartDaoImpl.checkoutCart();
	}

	@Override
	public void clearCart() {
		refShoppingCartDaoImpl.clearCart();
	}

}
